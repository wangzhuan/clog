#!/usr/bin/env bash

cd /data/server/logs

files=`ls *.error.log`
tag=`date +%Y%m%d%H`

for log_name in ${files}
do
    service_service_name=${log_name%%.error.log}
    service_service_name_len=${#service_service_name}
    service_name=${service_service_name:0:`expr ${service_service_name_len} / 2`}
    # echo $service_name
    # ps aux|grep /data/server/xngo/${service_name}/|grep -v grep
    service_pid=`ps aux|grep /data/server/xngo/${service_name}/|grep -v grep|awk '{print $2}'`
    # echo $service_pid
    if [[ ! -z ${service_pid} ]]; then
        # echo ${service_name}-${service_pid}
        file_name_len=${#log_name}
        log_rotate_file_name=${log_name:0:`expr ${file_name_len} - 4`}-${tag}.log
        if [[ ! -f ${log_rotate_file_name} ]]; then
            owner=$(stat -c %U.%G ${log_name})
            echo  "mv ${log_name} ${log_rotate_file_name}"
            touch ${log_name}.new
            chmod 664 ${log_name}.new
            chown ${owner} ${log_name}.new
            mv ${log_name} ${log_rotate_file_name} && mv ${log_name}.new ${log_name}
            kill -USR1 ${service_pid}
            echo "kill -USR1 ${service_pid};${service_name}"
        fi
    fi
done