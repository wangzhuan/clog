// +build windows

package clog

import (
	"os/signal"
	"syscall"
	"log"
)

func HandleSignal(lr *File) {
	signal.Notify(lr.signal, syscall.SIGALRM)

	for _ = range lr.signal {
		log.Printf("[INFO] Reopening %q\n", lr.path)
		if err := lr.reopen(); err != nil {
			log.Printf("[ERROR] error reopening: %s\n", err)
		}
	}
}
